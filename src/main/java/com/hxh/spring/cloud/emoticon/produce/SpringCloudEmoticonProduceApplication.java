package com.hxh.spring.cloud.emoticon.produce;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author huangxunhui
 * Date: Created in 2019-05-05 15:51
 * Utils: Intellij Idea
 * Description: SpringCloud Emoticon Produce 启动类
 */
@EnableAsync
@EnableEurekaClient
@SpringBootApplication
@MapperScan("com.hxh.spring.cloud.emoticon.produce.mapper")
public class SpringCloudEmoticonProduceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudEmoticonProduceApplication.class, args);
    }

}
