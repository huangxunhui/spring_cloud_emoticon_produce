package com.hxh.spring.cloud.emoticon.produce.config;

import org.springframework.stereotype.Component;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author huangxunhui
 * Date: Created in 2019-05-09 14:22
 * Utils: Intellij Idea
 * Description: 线程池配置
 */
@Component
public class ThreadPoolConfig {

    public ThreadPoolExecutor getThreadPool(){
        return new ThreadPoolExecutor( 1, 1, 10L, TimeUnit.SECONDS,
               new LinkedBlockingDeque<>(500),
               new ThreadPoolExecutor.CallerRunsPolicy());
    }
}
