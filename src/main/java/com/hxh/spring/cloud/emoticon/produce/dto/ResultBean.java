package com.hxh.spring.cloud.emoticon.produce.dto;

import lombok.Data;

/**
 * @author huangxunhui
 * Date: Created in 2019-05-09 18:06
 * Utils: Intellij Idea
 * Description:
 */
@Data
public class ResultBean {

    private String packName;

    private String imageUrl;

    private String pTime;
}
