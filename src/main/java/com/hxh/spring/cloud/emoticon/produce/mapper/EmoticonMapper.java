package com.hxh.spring.cloud.emoticon.produce.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hxh.spring.cloud.emoticon.produce.entity.Emoticon;
import org.springframework.stereotype.Repository;

/**
 * @author huangxunhui
 * Date: Created in 2019-05-08 11:53
 * Utils: Intellij Idea
 * Description:
 */
@Repository
public interface EmoticonMapper extends BaseMapper<Emoticon> {

}
