package com.hxh.spring.cloud.emoticon.produce.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hxh.spring.cloud.emoticon.produce.entity.Emoticon;

/**
 * @author huangxunhui
 * Date: Created in 2019-05-08 11:54
 * Utils: Intellij Idea
 * Description:
 */
public interface EmoticonService extends IService<Emoticon> {
}
