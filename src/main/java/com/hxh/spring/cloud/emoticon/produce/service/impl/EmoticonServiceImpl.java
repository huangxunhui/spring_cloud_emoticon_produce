package com.hxh.spring.cloud.emoticon.produce.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hxh.spring.cloud.emoticon.produce.entity.Emoticon;
import com.hxh.spring.cloud.emoticon.produce.mapper.EmoticonMapper;
import com.hxh.spring.cloud.emoticon.produce.service.EmoticonService;
import org.springframework.stereotype.Service;

/**
 * @author huangxunhui
 * Date: Created in 2019-05-08 11:55
 * Utils: Intellij Idea
 * Description:
 */
@Service
public class EmoticonServiceImpl extends ServiceImpl<EmoticonMapper , Emoticon> implements EmoticonService {

}
