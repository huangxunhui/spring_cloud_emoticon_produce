package com.hxh.spring.cloud.emoticon.produce.thread;

import com.hxh.spring.cloud.emoticon.produce.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * @author huangxunhui
 * Date: Created in 2019-05-16 22:32
 * Utils: Intellij Idea
 * Description:
 */
@Slf4j
public class ProducePhone implements Runnable {

    private String url;

    private RedisUtil<String> redisUtil;

    public ProducePhone(String url, RedisUtil<String> redisUtil) {
        this.url = url;
        this.redisUtil = redisUtil;
    }

    @Override
    public void run() {
        Document doc;
        try {
            ArrayList<String> phones = new ArrayList<>();
            doc = Jsoup.connect(url).timeout(15000).get();
            Elements elements = doc.getElementsByClass("deanthreads_author");
            elements.forEach(element -> {
                String username = element.text();
                if(checkPhone(username)){
                    log.info("phone:{}",username);
                    phones.add(username);
                }
            });
            redisUtil.setList("phone" ,phones);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private Boolean checkPhone(String phone){
        String regx = "^(((13[0-9])|(14[5-7])|(15[0-9])|(17[0-9])|(18[0-9]))+\\d{8})$";
        return Pattern.matches(regx, phone);
    }

}
