package com.hxh.spring.cloud.emoticon.produce.thread;

import com.hxh.spring.cloud.emoticon.produce.utils.RedisUtil;

/**
 * @author huangxunhui
 * Date: Created in 2019-05-10 14:53
 * Utils: Intellij Idea
 * Description:
 */
public class SaveUrlThread implements Runnable {

    private String url;

    private RedisUtil<String> redisUtil;

    public SaveUrlThread( RedisUtil<String> redisUtil , String url) {
        this.url = url;
        this.redisUtil = redisUtil;
    }

    @Override
    public void run() {
        redisUtil.setSet("URL" , url);
    }
}
