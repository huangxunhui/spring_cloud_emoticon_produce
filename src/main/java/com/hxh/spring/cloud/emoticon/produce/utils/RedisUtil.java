package com.hxh.spring.cloud.emoticon.produce.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author huangxunhui
 * Date: Created in 2018/2/7 14:19
 * Utils: Intellij Idea
 * Description: redis操作
 */
@Repository
public class RedisUtil<T> {

    private RedisTemplate<String,T> redisTemplate;

    @Autowired(required = false)
    public void setRedisTemplate(RedisTemplate<String,T> redisTemplate) {
        RedisSerializer stringSerializer = new StringRedisSerializer();
        Jackson2JsonRedisSerializer redisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setValueSerializer(redisSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(redisSerializer);
        this.redisTemplate = redisTemplate;
    }

    /**
     * 设置缓存
     * @param key 键
     * @param value 值
     */
    public void setKey(String key, T value) {
        ValueOperations<String,T> ops = redisTemplate.opsForValue();
        ops.set(key, value);
    }

    /**
     * 设置缓存
     * @param key 键
     * @param value 值
     * @param timeOut 过期时间
     * @param timeUnit 时间类型
     */
    public void setKey(String key, T value, Integer timeOut, TimeUnit timeUnit) {
        ValueOperations<String, T> ops = redisTemplate.opsForValue();
        ops.set(key, value, timeOut, timeUnit);
    }

    /**
     * 获取缓存里面的参数
     * @param key 键
     * @return T
     */
    public T getValue(String key) {
        ValueOperations<String, T> ops = redisTemplate.opsForValue();
        return ops.get(key);
    }

    public Long incrBy (String key , Long v){
        return redisTemplate.opsForValue().increment(key ,v);
    }

    /**
     * 设置hash
     * @param key key
     * @param hashKey hashKey
     * @param t 数据
     */
    public void setHash(String key ,String hashKey , T t){
        HashOperations<String, String, T> ops = redisTemplate.opsForHash();
        ops.put(key , hashKey , t);
    }

    /**
     * 删除Hash里面的数据
     * @param key key
     * @param hashKey hashKey
     */
    public void deleteHash(String key ,String hashKey){
        HashOperations<String, String, T> ops = redisTemplate.opsForHash();
        ops.delete(key,hashKey);
    }

    public Long setSet(String key , T... t){
        return redisTemplate.opsForSet().add(key, t);
    }

    public  Set<T> getSet(String key){
        return redisTemplate.opsForSet().members(key);
    }


    public Boolean setList(String key , List<T> list){

        ListOperations<String, T> opsForList = redisTemplate.opsForList();

        // 添加数据
        Long ret = opsForList.rightPushAll(key, list);
        if(ret == null ){
            return false;
        }else{
           return ret == list.size();
        }
    }

    /**
     * 获取hash 表内所有的值
     * @param key key
     * @return list
     */
    public List<T>  getHashAllValue(String key){
        HashOperations<String, String, T> ops = redisTemplate.opsForHash();
        return ops.values(key);
    }

    /**
     * 删除redis 里面的数据
     * @param key 键
     * @return 返回true 或者 false
     */
    public Boolean del(String key) {
        return redisTemplate.delete(key);
    }
}
