package com.hxh.spring.cloud.emoticon.produce;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringCloudEmoticonProduceApplicationTests {

    @Test
    public void contextLoads() {
    }

}
